package com.jlcindia.spring;

import javax.annotation.Resource;
//modified

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBean {
	
	@Resource(name = "oracleDS")
	DataSource oracleDataSource;
	
	@Resource(name = "mysqlDS")
	DataSource mysqlDataSource;
	
	
    {
		
		System.out.println("IB in TestBean");
	}
	
	public void show() {
		
		System.out.println("show() in TestBean");
		System.out.println(oracleDataSource);
		System.out.println(mysqlDataSource);
	}

	

}
